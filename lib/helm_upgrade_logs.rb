# frozen_string_literal: true

require_relative "helm_upgrade_logs/version"

module HelmUpgradeLogs
  class Error < StandardError; end
  # Your code goes here...
end
